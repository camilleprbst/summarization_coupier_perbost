# Génération automatique de résumés

## Etudiants : Camille Perbost et Lucien Coupier

## Dataset :

Nous utilisons le dataset Newsroom. Pour y accéder, il est nécessaire de faire une demande d'utilisation sur
[l'url suivante](https://cornell.qualtrics.com/jfe/form/SV_6YA3HQ2p75XH4IR)

Un lien d'accès vous sera donné instantanément par email.

Téléchargez les données, décompressez les pour obtenir les fichiers :
- train.jsonl
- dev.jsonl
- test.jsonl

Placez les à la racine du projet.
Vous aurez également besoin d'installer le package associé à ce dataset :

`pip install -e git+git://github.com/clic-lab/newsroom.git#egg=newsroom`

## Code :
Le package preprocessing contient le code nécessaire au chargement, au nettoyage et à la tokenization des données.

Le package model contient le code nécessaire à l'initialisation du modèle, à son entraînement et à son évaluation.

Afin de charger les données, d'entraîner le modèle et d'en évaluer la performance, exécutez le script main.py à la racine du projet.




