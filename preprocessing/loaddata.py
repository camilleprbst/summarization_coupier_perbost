#----- IMPORTS -----
from newsroom import jsonl
import pandas as pd
import os
from torchtext.data import get_tokenizer
import re
from nltk import download
download('stopwords')
from nltk.corpus import stopwords
import matplotlib.pyplot as plt


class LoadDataset:
    # currency regex
    CURRENCIES = {
        "$": "USD", "zł": "PLN", "£": "GBP", "¥": "JPY", "฿": "THB", "₡": "CRC", "₦": "NGN","₩": "KRW",
        "₪": "ILS", "₫": "VND", "€": "EUR", "₱": "PHP", "₲": "PYG", "₴": "UAH", "₹": "INR"}

    CURRENCY_REGEX = re.compile(
        "({})+".format("|".join(re.escape(c) for c in CURRENCIES.keys())))

    # email regex
    EMAIL_REGEX = re.compile(
        r"(?:^|(?<=[^\w@.)]))([\w+-](\.(?!\.))?)*?[\w+-]@(?:\w-?)*?\w+(\.([a-z]{2,})){1,3}(?:$|(?=\b))",
        flags=re.IGNORECASE | re.UNICODE,)

    # list of contraction (source: http://stackoverflow.com/questions/19790188/expanding-english-language-contractions-in-python)
    CONTRACTIONS = {"ain't": "is not", "aren't": "are not","can't": "cannot", "'cause": "because", "could've": "could have", "couldn't": "could not",
           "didn't": "did not",  "doesn't": "does not", "don't": "do not", "hadn't": "had not", "hasn't": "has not", "haven't": "have not",
           "he'd": "he would","he'll": "he will", "he's": "he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will", "how's": "how is",
           "I'd": "I would", "I'd've": "I would have", "I'll": "I will", "I'll've": "I will have","I'm": "I am", "I've": "I have", "i'd": "i would",
           "i'd've": "i would have", "i'll": "i will",  "i'll've": "i will have","i'm": "i am", "i've": "i have", "isn't": "is not", "it'd": "it would",
           "it'd've": "it would have", "it'll": "it will", "it'll've": "it will have","it's": "it is", "let's": "let us", "ma'am": "madam",
           "mayn't": "may not", "might've": "might have","mightn't": "might not","mightn't've": "might not have", "must've": "must have",
           "mustn't": "must not", "mustn't've": "must not have", "needn't": "need not", "needn't've": "need not have","o'clock": "of the clock",
           "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not", "sha'n't": "shall not", "shan't've": "shall not have",
           "she'd": "she would", "she'd've": "she would have", "she'll": "she will", "she'll've": "she will have", "she's": "she is",
           "should've": "should have", "shouldn't": "should not", "shouldn't've": "should not have", "so've": "so have","so's": "so as",
           "this's": "this is","that'd": "that would", "that'd've": "that would have", "that's": "that is", "there'd": "there would",
           "there'd've": "there would have", "there's": "there is", "here's": "here is","they'd": "they would", "they'd've": "they would have",
           "they'll": "they will", "they'll've": "they will have", "they're": "they are", "they've": "they have", "to've": "to have",
           "wasn't": "was not", "we'd": "we would", "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have", "we're": "we are",
           "we've": "we have", "weren't": "were not", "what'll": "what will", "what'll've": "what will have", "what're": "what are",
           "what's": "what is", "what've": "what have", "when's": "when is", "when've": "when have", "where'd": "where did", "where's": "where is",
           "where've": "where have", "who'll": "who will", "who'll've": "who will have", "who's": "who is", "who've": "who have",
           "why's": "why is", "why've": "why have", "will've": "will have", "won't": "will not", "won't've": "will not have",
           "would've": "would have", "wouldn't": "would not", "wouldn't've": "would not have", "y'all": "you all",
           "y'all'd": "you all would","y'all'd've": "you all would have","y'all're": "you all are","y'all've": "you all have",
           "you'd": "you would", "you'd've": "you would have", "you'll": "you will", "you'll've": "you will have",
           "you're": "you are", "you've": "you have", "i've": "i have"}

    def __init__(self, datasets_path, max_length_text = 150, max_text_summary = 100):
        self.datasets_path = datasets_path
        self.max_length_text = max_length_text
        self.max_test_summary = max_text_summary

    def load_data(self, train_size, val_size, test_size):
        #Read train file entry by entry
        tokenizer = get_tokenizer("spacy")
        train_data = {"summary": [], "text": []}
        print("Loading Train set...")
        with jsonl.open(os.path.join(self.datasets_path, "train.jsonl"), gzip = False) as train_file:
            i = 0
            for entry in train_file:
                text = entry["text"]
                summary = entry["summary"]
                if len(tokenizer(text)) > self.max_length_text:
                    continue
                if len(tokenizer(summary)) > self.max_test_summary:
                    continue
                train_data["summary"].append(summary)
                train_data["text"].append(text)
                # for testing purpose
                if i == train_size:
                    break
                i += 1
        print("Train set loaded.")

        # read validation file entry by entry
        print("Loading Validation set...")
        validation_data = {"summary": [], "text": []}
        with jsonl.open(os.path.join(self.datasets_path, "dev.jsonl"), gzip = False) as validation_file:
            i = 0
            for entry in validation_file:
                text = entry["text"]
                summary = entry["summary"]
                if len(tokenizer(text)) > self.max_length_text:
                    continue
                if len(tokenizer(summary)) > self.max_test_summary:
                    continue
                validation_data["summary"].append(summary)
                validation_data["text"].append(text)
                # for testing purpose
                if i == val_size:
                    break
                i += 1
        print("Val set loaded.")


        # read test file entry by entry
        test_data = {"summary": [], "text": []}
        print("Loading Test set...")
        with jsonl.open(os.path.join(self.datasets_path, "test.jsonl"), gzip = False) as test_file:
            i = 0
            for entry in test_file:
                text = entry["text"]
                summary = entry["summary"]
                if len(tokenizer(text)) > self.max_length_text:
                    continue
                if len(tokenizer(summary)) > self.max_test_summary:
                    continue
                test_data["summary"].append(summary)
                test_data["text"].append(text)
                # for testing purpose
                if i == test_size:
                    break
                i += 1
        print("Test set loaded.")

        # create DataFrame
        self.df_train = pd.DataFrame(train_data) 
        self.df_validation = pd.DataFrame(validation_data)
        self.df_test = pd.DataFrame(test_data)

    def clean_text(self, text, remove_stopword=False):
        text = text.lower()
            
        text = " ".join([LoadDataset.CONTRACTIONS[t] if t in LoadDataset.CONTRACTIONS else t for t in text.split(" ")])
        text = LoadDataset.EMAIL_REGEX.sub(" ", text)
        text = LoadDataset.CURRENCY_REGEX.sub(" ", text)
        text = re.sub(r'https?:\/\/.*[\r\n]*', '', text, flags=re.MULTILINE)
        # remove punctuation but break some company names like I.B.M
        text = re.sub(r'[_"\-;%()|+&=*%.,!?:#$@\[\]/]', ' ', text)
        text = re.sub(r"'s\b","", text)
        text = re.sub(r'&amp;', '', text)
            
        if remove_stopword == True:
            text = text.split()
            stop_words = set(stopwords.words("english"))
            text = [w for w in text if not w in stop_words]
            text = " ".join(text)
                
        return text

    def clean_df(self, df):
        df["summary"] = df["summary"].apply(lambda text: self.clean_text(text))
        df["text"] = df["text"].apply(lambda summ: self.clean_text(summ))
        return df

    def clean_dataset(self, remove_stopword=False):
        self.df_train = self.clean_df(self.df_train)
        self.df_validation = self.clean_df(self.df_validation)
        self.df_test = self.clean_df(self.df_test)
        print("Datasets cleaned")
    
    #Statistics about the cleaned datasets
    def analysis(self):
        word_count = {"summary":[], "text":[], "ratio":[]}
        for i in range(len(self.df_train)):
            text = self.df_train["text"][i]
            summary = self.df_train["summary"][i]
            word_count["summary"].append(len(summary.split()))
            word_count["text"].append(len(text.split()))
            word_count["ratio"].append(len(text.split())/(1+len(summary.split())))
            
        df_word_count = pd.DataFrame(word_count)
        df_word_count.hist(bins=30)
        plt.show()

        # % of texts with less than 200 words or 500 words
        count1 = 0
        count2 = 0
        for text in self.df_train["text"]:
            if (len(text.split()) < 200):
                count1 += 1
            if (len(text.split()) < 500):
                count2 += 1

        print("----------------------------------------")
        print("% of text with less than 200 words:", count1/len(self.df_train["text"]))
        print("% of text with less than 500 words:", count2/len(self.df_train["text"]))

        # % of summary with less than 30 words or 100 words
        count1 = 0
        count2 = 0
        for summary in self.df_train["summary"]:
            if (len(summary.split()) < 30):
                count1 += 1
            if (len(summary.split()) < 100):
                count2 += 1
        print("-"*40)
        print("% of summary with less than 30 words:", count1/len(self.df_train["summary"]))
        print("% of summary with less than 100 words:", count2/len(self.df_train["summary"]))
        print("----------------------------------------")

    def export_data(self):
        return self.df_train, self.df_validation, self.df_test

    
        

    
    

