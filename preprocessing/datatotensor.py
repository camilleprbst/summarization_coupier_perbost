#----- IMPORTS -----
from torchtext.data import get_tokenizer
import torch

class Lang:
    def __init__(self, name, tokenizer=get_tokenizer("spacy")):
        self.name = name
        self.tokenizer = tokenizer
        self.word2index = {"SOS": 0, "EOS": 1, "[PAD]": 2, "UNK": 3}
        self.word2count = {}
        self.index2word = {0: "SOS", 1: "EOS", 2: "[PAD]", 3: "UNK"}
        self.n_words = 3 # count SOS, EOS and PAD
            
    def addSentence(self, sentence):
        for word in self.tokenizer(sentence):
            self.addWord(word)
                
    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1

class DataToTensor:
    def __init__(self, df_train, df_validation, df_test):
        self.x_train = df_train["text"]
        self.y_train = df_train["summary"]
        self.x_val = df_validation["text"]
        self.y_val = df_validation["summary"]
        self.x_test = df_test["text"]
        self.y_test = df_test["summary"]
        self.tokenizer = get_tokenizer("spacy")
        self.prepareData()
        self.set_longest_sequence_train()
        self.set_longest_sequence_val()
        self.padd_data()
        self.set_tensors()

    @staticmethod    
    def readLangs(text, summary):
        pairs = [[text[i], summary[i]] for i in range(len(text))]
        input_lang = Lang(text)
        output_lang = Lang(summary)
        
        return input_lang, output_lang, pairs

    def prepareData(self):
        input_lang, output_lang, pairs = self.readLangs(self.x_train, self.y_train)
        val_pairs = [[self.x_val[i], self.y_val[i]] for i in range(len(self.x_val))]
        test_pairs = [[self.x_test[i], self.y_test[i]] for i in range(len(self.x_test))]
        print("Read %s sentence pairs"% len(pairs))
        print("Read %s validation sentence pairs"% len(val_pairs))
        print("Read %s test sentence pairs"% len(test_pairs))
        for pair in pairs:
            input_lang.addSentence(pair[0])
            output_lang.addSentence(pair[1])
        self.input_lang = input_lang
        self.output_lang = output_lang
        self.pairs = pairs
        self.val_pairs = val_pairs
        self.test_pairs = test_pairs   

    #longest sentence for train
    def set_longest_sequence_train(self):
        max_text_train = 0
        max_summ_train = 0
        for i in range(len(self.x_train)):
            text = self.x_train[i]
            summ = self.y_train[i]
            len_text = len(self.tokenizer(text))
            len_summ = len(self.tokenizer(summ))
            if len_text > max_text_train:
                max_text_train = len_text
            if len_summ > max_summ_train:
                max_summ_train = len_summ
        print("Max length for text test dataset: ", max_text_train)
        print("Max length for summ test dataset: ", max_summ_train)
        self.max_text_train = max_text_train
        self.max_summ_train = max_summ_train

    #longest sentence for validation
    def set_longest_sequence_val(self):
        max_text_val = 0
        max_summ_val = 0
        for i in range(len(self.x_val)):
            text = self.x_val[i]
            summ = self.y_val[i]
            len_text = len(self.tokenizer(text))
            len_summ = len(self.tokenizer(summ))
            if len_text > max_text_val:
                max_text_val = len_text
            if len_summ > max_summ_val:
                max_summ_val = len_summ
        print("Max length for text val dataset: ", max_text_val)
        print("Max length for summ val dataset: ", max_summ_val)
        self.max_text_val = max_text_val
        self.max_summ_val = max_summ_val

    def pad_sentence(self, pairs):
        padded_pairs = []
        text_len_list = []
        summ_len_list = []
        for i, pair in enumerate(pairs):
            text = pair[0]
            summ = pair[1]
            tokenized_text = self.tokenizer(text)
            tokenized_summ = self.tokenizer(summ)
            len_text = len(tokenized_text)
            len_summ = len(tokenized_summ)
            if len_summ <= len_text:
                tokenized_text.append("EOS")
                tokenized_summ.append("EOS")
                if len_text > self.max_text_train:
                    tokenized_text = tokenized_text[:self.max_text_train] + ["EOS"]
                    text_len_list.append(self.max_text_train + 1)
                if len_summ > self.max_summ_train:
                    tokenized_summ = tokenized_summ[:self.max_summ_train] + ["EOS"]
                    summ_len_list.append(self.max_summ_train + 1)
                if len_text < self.max_text_train + 1:
                    pad = ["[PAD]"] * (self.max_text_train - len_text)
                    tokenized_text += pad
                    text_len_list.append(len_text + 1)
                if len_summ < self.max_summ_train + 1:
                    pad = ["[PAD]"] * (self.max_summ_train - len_summ)
                    tokenized_summ += pad
                    summ_len_list.append(len_summ + 1)
                padded_pairs.append([tokenized_text, tokenized_summ])
        text_len_tensor = torch.tensor(text_len_list, dtype=torch.int32) #device
        summ_len_tensor = torch.tensor(summ_len_list, dtype=torch.int32) #device
        return padded_pairs, text_len_tensor, summ_len_tensor
    
    #padd sentences to give them the same size
    def padd_data(self):
        self.padded_pairs, self.text_len_tensor, self.summ_len_tensor = self.pad_sentence(self.pairs)
        self.padded_val_pairs, self.text_len_tensor_val, self.summ_len_tensor_val = self.pad_sentence(self.val_pairs)
        self.padded_test_pairs, self.text_len_tensor_test, self.summ_len_tensor_test = self.pad_sentence(self.test_pairs)
        print("{}/{} pairs remaining".format(len(self.padded_pairs), len(self.pairs)))
        print("{}/{} validation pairs remaining".format(len(self.padded_val_pairs), len(self.val_pairs)))
        print("{}/{} test pairs remaining".format(len(self.padded_test_pairs), len(self.test_pairs)))
        self.num_train_data = len(self.padded_pairs)
        self.num_val_data = len(self.padded_val_pairs)
        self.num_test_data = len(self.padded_test_pairs)
        self.len_text_sequence = self.max_text_train + 1
        self.len_summary_sequence = self.max_summ_train + 1
    
    def indexesFromSentence(self, lang, sentence):
        return [lang.word2index[word] if word in lang.word2index else 3 for word in sentence]

    def tensorFromSequence(self, lang, sentence):
        indexes = self.indexesFromSentence(lang, sentence)
        #indexes.append(EOS_token)
        return torch.tensor(indexes, dtype=torch.long) #device

    def tensorsFromPair(self, pair):
        input_tensor = self.tensorFromSequence(self.input_lang, pair[0])
        target_tensor = self.tensorFromSequence(self.output_lang, pair[1])
        return input_tensor, target_tensor

    #create tensors for the train
    def set_tensors(self):
        self.text_tensor = torch.zeros([self.num_train_data, self.len_text_sequence], dtype=torch.long)
        self.summary_tensor = torch.zeros([self.num_train_data, self.len_summary_sequence], dtype=torch.long)

        for i, pair in enumerate(self.padded_pairs):
            input_tensor, output_tensor = self.tensorsFromPair(pair)
            self.text_tensor[i] = input_tensor
            self.summary_tensor[i] = output_tensor

        # tokenisation validation
        self.text_tensor_val = torch.zeros([self.num_val_data, self.len_text_sequence], dtype=torch.long)
        self.summary_tensor_val = torch.zeros([self.num_val_data, self.len_summary_sequence], dtype=torch.long)

        for i, pair in enumerate(self.padded_val_pairs):
            input_tensor, output_tensor = self.tensorsFromPair(pair)
            self.text_tensor_val[i] = input_tensor
            self.summary_tensor_val[i] = output_tensor
            
        # tokenisation test
        self.text_tensor_test = torch.zeros([self.num_test_data, self.len_text_sequence], dtype=torch.long)
        self.summary_tensor_test = torch.zeros([self.num_test_data, self.len_summary_sequence], dtype=torch.long)

        for i, pair in enumerate(self.padded_test_pairs):
            input_tensor, output_tensor = self.tensorsFromPair(pair)
            self.text_tensor_test[i] = input_tensor
            self.summary_tensor_test[i] = output_tensor