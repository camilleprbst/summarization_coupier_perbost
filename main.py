#----- IMPORTS -----
from preprocessing.loaddata import LoadDataset
from preprocessing.datatotensor import DataToTensor
from model.models import Embedding
from model.models import Seq2Seq
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader
import math

if __name__ == "__main__":

    #----- LOAD DATASET -----

    #Parameters
    path = "/Users/camilleperbost/Documents/Cours/3A/Deep Learning/Summarization/"
    text_max_len = 200
    summ_max_len = 100
    train_size = 100
    val_size = 10
    test_size = 10
    #Additionnal tokens
    SOS_token = 0
    EOS_token = 1
    PAD_token = 2
    UNK_token = 3


    #Load data
    loader = LoadDataset(path, text_max_len, summ_max_len)
    loader.load_data(train_size, val_size, test_size)
    #Clean data
    loader.clean_dataset()
    #Statistics about resulting data
    loader.analysis()
    #Conversion of data to tensors
    Tensors = DataToTensor(loader.df_train, loader.df_validation, loader.df_test)
    #Creation of input and output embeddings
    Embeddings = Embedding(Tensors.input_lang, Tensors.output_lang)

    #----- MODEL -----

    #parameters
    params = {}
    params["enc_hidden_size"] = 512
    params["dec_hidden_size"] = 512
    params["input_size"] = Tensors.input_lang.n_words
    params["output_size"] = Tensors.output_lang.n_words
    params["embedding_size"] = 300
    params["n_layers"] = 1
    params["enc_dropout"] = 0.5
    params["dec_dropout"] = 0.5

    #Set model
    model = Seq2Seq(params, Embeddings.input_embedding, Embeddings.output_embedding)
    
    #Masks
    #Train
    mask = Tensors.text_tensor.clone()
    mask[mask != 2] = 1
    mask[mask == 2] = 0
    #Val
    mask_val = Tensors.text_tensor_val.clone()
    mask_val[mask_val != 2] = 1
    mask_val[mask_val == 2] = 0
    #Test
    mask_test = Tensors.text_tensor_test.clone()
    mask_test[mask_test != 2] = 1
    mask_test[mask_test == 2] = 0

    #Optimizer
    param_to_optimize = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = optim.Adam(param_to_optimize, lr=0.0005)

    #Criterion and PAD token
    criterion = nn.CrossEntropyLoss(ignore_index = PAD_token)

    #Batch terators
    BATCH_SIZE = 16
    #Train
    train_dataset = TensorDataset(Tensors.text_tensor, Tensors.summary_tensor, Tensors.text_len_tensor, mask)
    train_loader = DataLoader(
        dataset=train_dataset,
        batch_size=BATCH_SIZE,
        shuffle=True
    )
    #Val
    val_dataset = TensorDataset(Tensors.text_tensor_val, Tensors.summary_tensor_val, Tensors.text_len_tensor_val, mask_val)
    val_loader = DataLoader(
        dataset=val_dataset,
        batch_size=BATCH_SIZE,
        shuffle=True
    )
    #Test
    test_dataset = TensorDataset(Tensors.text_tensor_test, Tensors.summary_tensor_test, Tensors.text_len_tensor_test, mask_test)
    test_loader = DataLoader(
        dataset=test_dataset,
        batch_size=BATCH_SIZE,
        shuffle=True
    )

    #----- TRAIN -----
    #Parameters
    N_EPOCH = 10
    CLIP = 1

    best_val_loss = float("inf")

    for epoch in range(N_EPOCH):
        start_time = time.time()
        #Train loss
        train_loss = model.train1(train_loader, optimizer, criterion, CLIP)
        #Val loss
        val_loss = model.evaluate1(val_loader, criterion)
        end_time = time.time()
        epoch_mins, epoch_secs = model.epoch_time(start_time, end_time)
        
        #Save best model for future inference
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            torch.save(model.state_dict(), 'model_enc_dec_1.pt')
            
        print(f'Epoch: {epoch+1:02} | Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f} | Train PPL: {math.exp(train_loss):7.3f}')
        print(f'\t Val. Loss: {val_loss:.3f} |  Val. PPL: {math.exp(val_loss):7.3f}')
    

    #----- INFERENCE AND GENERALIZATION -----
    test_loss = model.evaluate1(test_loader, criterion)
    print(f'| Test Loss: {test_loss:.3f} | Test PPL: {math.exp(test_loss):7.3f} |')

    summary_pred, text_sentence = model.generate_summary(Tensors.text_tensor_test, Tensors.summary_tensor_test, Tensors.input_lang, Tensors.output_lang, mask_test)
