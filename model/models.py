#----- IMPORTS -----
import spacy.cli
spacy.cli.download("en_core_web_lg")
import en_core_web_lg
import numpy as np
import torch
import torch.nn as nn
from tqdm import tqdm
import torch.nn.functional as F
import random
from random import randrange

class EncoderGRU(nn.Module):
    def __init__(self, params, input_embedding):
        super(EncoderGRU, self).__init__()
        self.enc_hidden_size = params["enc_hidden_size"]
        self.dec_hidden_size = params["dec_hidden_size"]
        self.input_size = params["input_size"]
        self.embedding_size = params["embedding_size"]
        self.n_layers = params["n_layers"]
        self.dropout = params["enc_dropout"]
            
        self.embedding = nn.Embedding(self.input_size, self.embedding_size)
        self.embedding.weight = nn.Parameter(input_embedding)
        self.embedding.weight.requires_grad = False
        self.gru = nn.GRU(self.embedding_size, self.dec_hidden_size, bidirectional=True)
        self.fc = nn.Linear(self.enc_hidden_size*2, self.dec_hidden_size)
        self.dropout = nn.Dropout(self.dropout)
            
    def forward(self, src, hidden, text_len_tensor):
        total_length = src.size()[0]
        emb = self.embedding(src)
        emb = self.dropout(emb)
        packed_emb = nn.utils.rnn.pack_padded_sequence(emb, text_len_tensor, enforce_sorted=False)
        packed_outputs, hidden = self.gru(packed_emb)
        outputs, _ = nn.utils.rnn.pad_packed_sequence(packed_outputs, total_length=total_length) 
        hidden = self.fc(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim=1))
        hidden = torch.tanh(hidden)
        return outputs, hidden
        
    def initHidden(self, batch_size):
        h_init = torch.randn(self.n_layers*2, batch_size, self.enc_hidden_size)
        return h_init

class Attention(nn.Module):
    def __init__(self, params):
        super(Attention, self).__init__()
        self.enc_hidden_size = params["enc_hidden_size"]
        self.dec_hidden_size = params["dec_hidden_size"]
            
        self.attn = nn.Linear((self.enc_hidden_size * 2) + self.dec_hidden_size, self.dec_hidden_size)
        self.v = nn.Linear(self.dec_hidden_size, 1, bias=False)
            
    def forward(self, hidden, encoder_outputs, mask):
        seq_len = encoder_outputs.size()[0]
        batch_size = encoder_outputs.size()[1]
        #duplicate hidden sel_len times
        hidden = hidden.unsqueeze(1).repeat(1, seq_len, 1)
        #permute encoder_outputs to have same shape as hidden
        #ie [batch_size, seq_len, dec_hidden_dim]
        encoder_outputs = encoder_outputs.permute(1, 0, 2)
        e = self.attn(torch.cat((hidden, encoder_outputs), dim=2))
        e = torch.tanh(e)
        attention = self.v(e).squeeze(2)
        #mask before softmax, pad value will be zero after softmax
        attention = attention.masked_fill(mask == 0, -1e10)
        return F.softmax(attention, dim=1)

class DecoderGRU(nn.Module):
    def __init__(self, params, attention, output_embedding):
        super(DecoderGRU, self).__init__()
        self.enc_hidden_size = params["enc_hidden_size"]
        self.dec_hidden_size = params["dec_hidden_size"]
        self.output_size = params["output_size"]
        self.embedding_size = params["embedding_size"]
        self.n_layers = params["n_layers"]
        self.dropout = params["dec_dropout"]
            
        self.embedding = nn.Embedding(self.output_size, self.embedding_size)
        self.embedding.weight = nn.Parameter(output_embedding)
        self.embedding.weight.requires_grad = False
        self.gru = nn.GRU((self.enc_hidden_size * 2) + self.embedding_size, self.dec_hidden_size)
        self.attention = attention
        self.fc_out = nn.Linear((self.enc_hidden_size * 2) + self.dec_hidden_size + self.embedding_size, self.output_size)
        self.dropout = nn.Dropout(self.dropout)

    def forward(self, src, hidden, encoder_outputs, mask):
        src = src.unsqueeze(0)
        embedded = self.dropout(self.embedding(src))
        a = self.attention(hidden, encoder_outputs, mask)
        a = a.unsqueeze(1)
        encoder_outputs = encoder_outputs.permute(1, 0, 2)
        weighted = torch.bmm(a, encoder_outputs)
        weighted = weighted.permute(1, 0, 2)
        gru_input = torch.cat((embedded, weighted), dim = 2)
        output, hidden = self.gru(gru_input, hidden.unsqueeze(0))
            
        embedded = embedded.squeeze(0)
        output = output.squeeze(0)
        weighted = weighted.squeeze(0)
            
        prediction = self.fc_out(torch.cat((output, weighted, embedded), dim = 1))
            
        return prediction, hidden.squeeze(0)

class Embedding:
    def __init__(self, input_lang, output_lang):
        self.set_embeddings(input_lang, output_lang)

    @staticmethod
    def create_spacy_embedding(lang, spacy_nlp):
        vocab_size = len(lang.index2word)
        word_vec_size = spacy_nlp.vocab.vectors_length
        embedding = np.zeros((vocab_size, word_vec_size))
        unk_count = 0
            
        print("Vocabulary size: {}".format(vocab_size))
        print("Word vector size: {}".format(word_vec_size))
                
        for token, index in lang.word2index.items():
            if token == "[PAD]":
                continue
            elif token in ["SOS", "EOS", "UNK"]:
                vector = np.random.rand(word_vec_size,)
            elif spacy_nlp.vocab[token].has_vector: 
                vector = spacy_nlp.vocab[token].vector
            else:
                vector = embedding[3]
                unk_count += 1
                    
            embedding[index] = vector
                
        print('- Unknown word count: {}'.format(unk_count))
            
        return torch.from_numpy(embedding).float()
    
    def set_embeddings(self, input_lang, output_lang):
        nlp = en_core_web_lg.load()
        self.input_embedding = self.create_spacy_embedding(input_lang, nlp)
        print("-"*25)
        self.output_embedding = self.create_spacy_embedding(output_lang, nlp)
        

class Seq2Seq(nn.Module):
    def __init__(self, params, input_embedding, output_embedding):
        super(Seq2Seq, self).__init__()
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.encoder = EncoderGRU(params, input_embedding).to(self.device)
        self.attention = Attention(params).to(self.device)
        self.decoder = DecoderGRU(params, self.attention, output_embedding).to(self.device)
        self.init_weights()

    def forward(self, batch_text, batch_summary, text_len_tensor, mask, teacher_forcing_ratio = 0.5):
        seq_size, batch_size = batch_summary.size()
        output_size = self.decoder.output_size
        outputs = torch.zeros(seq_size, batch_size, output_size).to(self.device)
        h_init = self.encoder.initHidden(batch_size).to(self.device)
        encoder_outputs, hidden = self.encoder(batch_text, h_init, text_len_tensor)
        #first input for decoder with <SOS> tokens
        input = torch.zeros(batch_size, dtype=int).to(self.device)
            
        for i in range(0, seq_size):
            output, hidden = self.decoder(input, hidden, encoder_outputs, mask)
            outputs[i] = output
                
            # teacher forcing
            tearcher_forcing = random.random() < teacher_forcing_ratio
                
            # highest prediction
            pred = output.argmax(1)
                
            # next input
            input = batch_summary[i] if tearcher_forcing else pred
            
        return outputs

    #train model
    def train1(self, iterator, optimizer, criterion, clip):
        self.train()
        epoch_loss = 0
            
        for i, (x, target, text_len_tensor, mask) in enumerate(tqdm(iterator)):

            x = x.to(self.device)
            target = target.to(self.device)
            mask = mask.to(self.device)
            x = x.transpose(0, 1)
            target = target.transpose(0, 1)

            optimizer.zero_grad()
            output = self(x, target, text_len_tensor, mask)
            output_dim = output.size()[-1]
            output = output.view(-1, output_dim)
            target = target.contiguous().view(-1)
                
            loss = criterion(output, target)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(self.parameters(), clip)
            optimizer.step()
            epoch_loss += loss.item()
                
        return epoch_loss / len(iterator)
    
    #evaluate model
    def evaluate1(self, iterator, criterion):
        self.eval()
        epoch_loss = 0
            
        with torch.no_grad():
            for i, (x, target, text_len_tensor, mask) in enumerate(iterator):
                x = x.to(self.device)
                target = target.transpose(0, 1)
                target = target.to(self.device)
                x = x.transpose(0, 1)
                mask = mask.to(self.device)
                    
                output = self(x, target, text_len_tensor, mask, 0)
                output_dim = output.size()[-1]
                output = output.view(-1, output_dim)
                target = target.contiguous().view(-1)
                    
                loss = criterion(output, target)
                    
                epoch_loss += loss.item()
                    
        return epoch_loss / len(iterator)
    
    #set weights of the seq2seq model
    def init_weights(self):
        for name, param in self.named_parameters():
            if param.requires_grad:
                if 'weight' in name:
                    nn.init.normal_(param.data, mean=0, std=0.01)
                else:
                    nn.init.constant_(param.data, 0)

    #count_number of parameters
    def count_parameters(self):
        self.nb_parameters = sum(p.numel() for p in self.parameters() if p.requires_grad)

    #measure epoch training time
    @staticmethod
    def epoch_time(start_time, end_time):
        elapsed_time = end_time - start_time
        elapsed_mins = int(elapsed_time / 60)
        elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
        return elapsed_mins, elapsed_secs

    #inference with a trained model 
    def generate_summary(self, text_tensor_test, summary_tensor_test, input_lang, output_lang, mask_test, max_len = 50):         
        self.eval()
            
        rd = randrange(len(text_tensor_test))
        text = text_tensor_test[rd]
        summary = summary_tensor_test[rd]
            
        text_len = torch.LongTensor([len(text)])
            
        print(mask_test[rd].size())
        mask_src = mask_test[rd]
            
        text_sentence = [input_lang.index2word[word.item()] for word in text if word.item() not in [0, 1, 2]]
        summary_sentence = [output_lang.index2word[word.item()] for word in summary if word.item() not in [0, 1, 2]]
            
        src_tensor = text.unsqueeze(1).to(self.device)
        with torch.no_grad():
            h_init = self.encoder.initHidden(1)
            encoder_outputs, hidden = self.encoder(src_tensor, h_init, text_len)
            
        attentions = torch.zeros(max_len, 1, len(text)).to(self.device)
            
        trg_preds = [input_lang.word2index["SOS"]]
            
        for i in range(max_len):
                
            trg_tensor = torch.LongTensor([trg_preds[-1]]).to(self.device)
            hidden = hidden.to(self.device)
            encoder_outputs = encoder_outputs.to(self.device)
            mask_src = mask_src.to(self.device)
            with torch.no_grad():
                output, hidden = self.decoder(trg_tensor, hidden, encoder_outputs, mask_src)
            #attentions[i] = attention
                
            pred_token = output.argmax(1).item()
            trg_preds.append(pred_token)
                
            if pred_token == input_lang.word2index["EOS"]:
                break
                    
        trg_sentence = [output_lang.index2word[token] for token in trg_preds]
                
        print("- Original Text - ")
        print(text_sentence)
        print("- Original Summary - ")
        print(summary_sentence)
        print("- Generated Summary - ")
        print(trg_sentence)
            
        return trg_sentence, text_sentence
    